class Nif:
    def __init__(self, n, l):
        self.numero = n
        self.letra = l

    def calcula_letra(self):
        diccionario = {0:"T",1:"R",2:"W",3:"A",4:"G",5:"M",6:"Y",7:"F",8:"P",9:"D",10:"X",11:"B",12:"N",13:"J",14:"Z",15:"S",16:"Q",17:"V",18:"H",19:"L",20:"C",21:"K",22:"E"}
        resto = self.numero%23
        self.letra = diccionario[resto]
        return self.letra


    def __str__(self):
        return "El dni con número {num} tiene como letra {letra}".format(num=self.numero,letra=self.calcula_letra())
  




 


