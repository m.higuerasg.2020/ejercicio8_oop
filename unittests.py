#!/usr/bin/env python3

import unittest
from nif import Nif

class TestNif(unittest.TestCase):

    def test_numero(self):
        d1 =Nif(51138221,"K")
        self.assertEqual(d1.numero, 51138221)

    def test_letra(self):
        d1 =Nif(51138221,"K")
        self.assertEqual(d1.calcula_letra(), "K")
   
    def test_str(self):
        d1 =Nif(51138221,"K")
        self.assertEqual("El dni con número 51138221 tiene como letra K", d1.__str__())

if __name__ == "__main__":
    unittest.main()
